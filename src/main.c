#include <config.h>
#include <gnome.h>
#include "gee-equation.h"
#include "gee-object.h"
#include "gee-word.h"
#include "gee-basic.h"
#include "gee-root.h"
#include "gee-sumprod.h"
#include "gee-canvas.h"

static int
prepend_a_1(gpointer data)
{
	static int i = 1;
	char *s;
	GeeObject *word;

	word = GEE_OBJECT(gtk_object_new(GEE_TYPE_WORD,"word","+",NULL));
	gee_basic_prepend_child(GEE_BASIC(data),word);
	s = g_strdup_printf("%d",i++);
	word = GEE_OBJECT(gtk_object_new(GEE_TYPE_WORD,"word",s,NULL));
	g_free(s);
	gee_basic_prepend_child(GEE_BASIC(data),word);
	return TRUE;
}

static int
add_a_1(gpointer data)
{
	static int i = 3;
	char *s;

	s = g_strdup_printf("%d",i++);
	gtk_object_set(GTK_OBJECT(data),
		       "word",s,
		       NULL);
	g_free(s);
	return TRUE;
}

static void
add_test_equation(GeeCanvas *canvas)
{
	GeeEquation *eq;
	GeeObject *basic;
	GeeObject *word;

	eq = gee_equation_new();

	basic = gee_basic_new();
	gee_equation_set_root(eq,basic);

	word = GEE_OBJECT(gtk_object_new(GEE_TYPE_WORD,"word","1",NULL));
	gee_basic_append_child(GEE_BASIC(basic),word);

	word = GEE_OBJECT(gtk_object_new(GEE_TYPE_WORD,"word","+",NULL));
	gee_basic_append_child(GEE_BASIC(basic),word);

	word = GEE_OBJECT(gtk_object_new(GEE_TYPE_WORD,"word","2",NULL));
	gee_basic_append_child(GEE_BASIC(basic),word);

	gtk_timeout_add(1000,add_a_1,word);

	gtk_object_set(GTK_OBJECT(canvas),"equation",eq,NULL);

	gtk_timeout_add(3000,prepend_a_1,basic);
}

/* A TEST program for the gee framework */
int
main(int argc, char *argv[])
{
	GtkWidget *app;
	GtkWidget *sw;
	GtkWidget *canvas;


	/* Initialize the i18n stuff */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	gnome_init ("gee", VERSION, argc, argv);
	
        app=gnome_app_new("gee", _("GNOME Equation Editor"));
	gtk_window_set_wmclass (GTK_WINDOW (app), "gee", "gee");

        gtk_signal_connect(GTK_OBJECT(app), "delete_event",
			   GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

	sw = gtk_scrolled_window_new(NULL,NULL);
	gnome_app_set_contents(GNOME_APP(app), sw);
	canvas = gee_canvas_new();
	gtk_container_add(GTK_CONTAINER(sw),canvas);

	gtk_window_set_default_size(GTK_WINDOW(app),200,200);

	add_test_equation(GEE_CANVAS(canvas));

	gtk_widget_show_all(app);

	gtk_main ();

	return 0;
}
