/* A word display object */
%h{
#include <config.h>
#include <gnome.h>
#include "gee-canvas-item.h"
%}

%{
#include "gee-basic.h"
#include "gee-object.h"
#include "gee-canvas.h"

#define VERT_GAP 5
#define HORIZ_GAP 5

static void object_append_child(GeeBasic *basic, GeeObject *object, GeeCanvasItem *item);
static void object_prepend_child(GeeBasic *basic, GeeObject *object, GeeCanvasItem *item);
static void object_insert_child(GeeBasic *basic, GeeObject *object, int pos, GeeCanvasItem *item);
%}

class Gee:Canvas:Basic from Gee:Canvas:Item {
	/* override (Gee:Canvas:Item)
	void
	redraw(Gee:Canvas:Item *self (check null type))
	{
	} */

	public
	void
	prepend_child(self, Gee:Canvas:Item *child (check null type))
	{
		GeeCanvasItem *item = GEE_CANVAS_ITEM(self);

		g_return_if_fail(!child->parent);

		item->children = g_list_prepend(item->children,child);
		gee_canvas_item_add_child(item,child);
	}

	public
	void
	append_child(self, Gee:Canvas:Item *child (check null type))
	{
		GeeCanvasItem *item = GEE_CANVAS_ITEM(self);

		g_return_if_fail(!child->parent);

		item->children = g_list_append(item->children,child);
		gee_canvas_item_add_child(item,child);
	}

	public
	void
	insert_child(self, Gee:Canvas:Item *child (check null type),
		     int pos (check >= 0))
	{
		GeeCanvasItem *item = GEE_CANVAS_ITEM(self);

		g_return_if_fail(!child->parent);

		item->children = g_list_insert(item->children,child,pos);
		gee_canvas_item_add_child(item,child);
	}

	override (Gee:Canvas:Item)
	void
	size_request(Gee:Canvas:Item *self (check null type),
		     GtkRequisition *req (check null))
	{
		GList *li;

		req->width = HORIZ_GAP;
		req->height = 0;

		for(li = self->children; li; li = li->next) {
			GtkRequisition chreq;
			GeeCanvasItem *item = GEE_CANVAS_ITEM(li->data);
			gee_canvas_item_size_request(item,&chreq);
			req->width += chreq.width + HORIZ_GAP;
			if(chreq.height > req->height)
				req->height = chreq.height;
		}
		req->height += VERT_GAP*2;

		PARENT_HANDLER(self,req);
	}

	override (Gee:Canvas:Item)
	void
	size_allocate(Gee:Canvas:Item *self (check null type),
		      GtkAllocation *alloc (check null))
	{
		GList *li;
		int x;

		PARENT_HANDLER(self,alloc);

		/*FIXME: center the children if we have more space */

		x = HORIZ_GAP;
		for(li = self->children; li; li = li->next) {
			GtkAllocation challoc;
			GeeCanvasItem *item = GEE_CANVAS_ITEM(li->data);
			challoc.x = x;
			challoc.y = VERT_GAP;
			challoc.width = item->requisition.width;
			challoc.height = item->requisition.height;
			gee_canvas_item_size_allocate(item,&challoc);
			x += item->requisition.width;
		}
	}

	override (Gee:Canvas:Item)
	void
	set_object(Gee:Canvas:Item *self (check null type), Gee:Object *object (check null type))
	{
		GeeCanvasBasic *basic = GEE_CANVAS_BASIC(self);
		GeeCanvas *canvas = GEE_CANVAS(GNOME_CANVAS_ITEM(self)->canvas);
		GList *li;

		gee_canvas_add_to_item_hash(canvas,object,self);

		for(li=g_list_last(object->children);li;li=li->prev) {
			GeeCanvasItem *item =
				gee_canvas_make_item_from_object(canvas,self,GEE_OBJECT(li->data));
			gee_canvas_basic_prepend_child(basic,item);
		}

		gtk_signal_connect_while_alive(GTK_OBJECT(object),"append_child",
					       GTK_SIGNAL_FUNC(object_append_child),
					       self,
					       GTK_OBJECT(self));
		gtk_signal_connect_while_alive(GTK_OBJECT(object),"prepend_child",
					       GTK_SIGNAL_FUNC(object_prepend_child),
					       self,
					       GTK_OBJECT(self));
		gtk_signal_connect_while_alive(GTK_OBJECT(object),"insert_child",
					       GTK_SIGNAL_FUNC(object_insert_child),
					       self,
					       GTK_OBJECT(self));


		gtk_signal_connect_object_while_alive(GTK_OBJECT(object),"changed",
						      GTK_SIGNAL_FUNC(object_changed),
						      GTK_OBJECT(self));

		PARENT_HANDLER(self,object);
	}
	private
	void
	object_changed(self)
	{
		GeeCanvas *canvas = GEE_CANVAS(GNOME_CANVAS_ITEM(self)->canvas);
		/*gee_canvas_item_redraw(GEE_CANVAS_ITEM(self));*/
		gee_canvas_queue_resize(canvas);
	}
}

%{
static void
object_append_child(GeeBasic *basic, GeeObject *object, GeeCanvasItem *item)
{
	GeeCanvas *canvas = GEE_CANVAS(GNOME_CANVAS_ITEM(item)->canvas);
	GeeCanvasItem *child =
		gee_canvas_make_item_from_object(canvas,item,GEE_OBJECT(object));
	gee_canvas_basic_append_child(GEE_CANVAS_BASIC(item),child);
}
static void
object_prepend_child(GeeBasic *basic, GeeObject *object, GeeCanvasItem *item)
{
	GeeCanvas *canvas = GEE_CANVAS(GNOME_CANVAS_ITEM(item)->canvas);
	GeeCanvasItem *child =
		gee_canvas_make_item_from_object(canvas,item,GEE_OBJECT(object));
	gee_canvas_basic_prepend_child(GEE_CANVAS_BASIC(item),child);
}
static void
object_insert_child(GeeBasic *basic, GeeObject *object, int pos, GeeCanvasItem *item)
{
	GeeCanvas *canvas = GEE_CANVAS(GNOME_CANVAS_ITEM(item)->canvas);
	GeeCanvasItem *child =
		gee_canvas_make_item_from_object(canvas,item,GEE_OBJECT(object));
	gee_canvas_basic_insert_child(GEE_CANVAS_BASIC(item),child,pos);
}
%}
